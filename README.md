# OpenML dataset: scpf

https://www.openml.org/d/41487

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Multivariate regression data set from: https://link.springer.com/article/10.1007%2Fs10994-016-5546-z : This is a pre-processed version of the dataset used in Kaggles See Click Predict Fix competition (Kaggle 2013). It concerns the prediction of three target variables that represent the number of views, clicks and comments that a specific 311 issue will receive. The issues have been collected from 4 cities (Oakland, Richmond, New Haven, Chicago) in the US and span a period of 12 months (01 2012-12 2012). The version of the dataset that we use here is a random 1 percent sample of the data. In terms of features we use the number of days that an issues stayed online, the source from where the issue was created (e.g. android, iphone, remote api, etc.), the type of the issue (e.g. graffiti, pothole, trash, etc.), the geographical co-ordinates of the issue, the city it was published from and the distance from the city center. All multi-valued nominal variables were first transformed to binary and then rare binary variables (being true for less than 1 percent of the cases) were removed.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41487) of an [OpenML dataset](https://www.openml.org/d/41487). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41487/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41487/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41487/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

